// Bài tập 1 "
{
    let sum = 0;
    let n = 1;

    while (sum <= 1000) {
    sum += n;
    n++;
    }
    ketQua = n - 1;
}
document.getElementById("result").innerHTML = `Số nguyên dương nhỏ nhất : n = ${ketQua}`;

// Bài tập 2 :
function tinhTong() {
    var n = document.getElementById("numN").value;
    var x = document.getElementById("numX").value;
    var sum = 0;
    var power = 1;
    
    for (var i = 1; i <= n; i++) {
        power *= x;  
        sum += power;
    }
   
    document.getElementById("result1").innerHTML = `Tổng S(n) = ${sum}`;
}

//Bài tập 3 :

function tinhGiaiThua() {
    var n = document.getElementById("numN1").value;
    var giaiThua = 1;
    for(var i = 1;i <= n;i++){
        giaiThua *= i;

    }
    document.getElementById("result2").innerHTML = ` Kết quả : ${giaiThua}`;
}

// Bài Tập 4 :

function taoDiv(){
    var taoDiv = document.getElementById("result3");
    for(var i = 1;i <= 10;i++) {
        var div = document.createElement("div");
        if( i % 2 === 0){
        div.style.backgroundColor = "red";
        }else{
        div.style.backgroundColor = "green";
       }

       div.textContent = ` Thẻ Div: ${i}`;
       taoDiv.appendChild(div);
    }

}   
